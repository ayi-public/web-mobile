
// Instancia el modulo de express.
var express = require('express'),
	app = express(),

// Se instancia la libreria node-jasper el cual falicita la generacion de reportes.
	jasper = require('node-jasper')({
		//Direccion de la libreria jasper. (Usar el .tar.gz extrido)
		path: 'lib/jasperreports-6.1.0',
		// Conexion del jasper (En este caso usara json por ende usara in_memory_json)
		// Ubicacion de los jrxml y el compilado (jasper)
    reports: {
        stock_ofertas: {
            jasper: __dirname + '/Sample_3.jasper',
            jrxml: __dirname + '/Sample_3.jrxml',
            conn: 'in_memory_json'
        }
    }
	});


	//Funcion que retorna el jasper.
	app.get('/pdf', function(req, res, next) {

		//Json Array de prueba
    var json =[
      {name:'Anna', lastname:'Smith'},
      {name:'Peter', lastname:'Jones'},
      {name:'Angola', lastname:'Langola'},
      {name:'Lamecha', lastname:'Pes'}
		]

		//Se llama previo a llamar la funcion que generara el jasper para pasarle los params y los field.
		var report = {

			//Nombre del reporte que se generara (Es el mismo que se configuro arriba)
			report: 'stock_ofertas',

			//Parametros del jasper (Tienen que tener el mismo nombre que los declarados en jasper.)
      data: {
                lenguaje: 'Español',
								titulo: "Tarjeta Naranja",
                // on jasper make a parameter named "dataset2" and use on a subreport:
                // ((net.sf.jasperreports.engine.data.JsonDataSource)$P{dataset2})
            },
			//Data Adaprter del jasper.
      dataset: json
		};


		//Se genera el jasper.
		var pdf = jasper.pdf(report);

		//Config de retorno
		res.set({
			'Content-type': 'application/pdf',
			'Content-Length': pdf.length
		});

		//Conversion del jasper a base 64
		pdfString = pdf.toString('base64');
		console.log(pdfString);

		//Retorno del PDF.
		res.send(pdf);
	});

app.listen(3000, function () {
    console.log("La app esta funcionando en el puerto 3000.");
});
