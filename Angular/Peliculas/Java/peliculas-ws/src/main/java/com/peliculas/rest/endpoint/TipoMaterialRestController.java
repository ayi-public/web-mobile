//package com.peliculas.rest.endpoint;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.peliculas.business.service.TipoMaterialService;
//import com.peliculas.entities.TipoMaterial;
//import com.peliculas.rest.endpoint.util.Response;
//
//@RestController
//@RequestMapping("/stl/tipomaterial")
//public class TipoMaterialRestController {
//
//	private static final String JSON_TYPE = MediaType.APPLICATION_JSON_VALUE;
//
//	@Autowired
//	private TipoMaterialService service;
//
//	@RequestMapping(value = "", method = RequestMethod.GET, produces = JSON_TYPE)
//	public ResponseEntity<Response<List<TipoMaterial>>> findAll() {
//		List<TipoMaterial> list = service.findAllJerarquia();
//		Response<List<TipoMaterial>> resp = new Response<List<TipoMaterial>>();
//		resp.setData(list);
//		ResponseEntity<Response<List<TipoMaterial>>> responseEntity = new ResponseEntity<Response<List<TipoMaterial>>>(
//				resp, HttpStatus.OK);
//		return responseEntity;
//	}
//
//	@RequestMapping(value = "", method = RequestMethod.POST, consumes = JSON_TYPE, produces = JSON_TYPE)
//	public ResponseEntity<Response> create(@RequestBody TipoMaterial o) {
//		service.create(o);
//		Response resp = new Response();
//		resp.setData(o);
//		ResponseEntity<Response> retorno = new ResponseEntity<Response>(resp, HttpStatus.OK);
//		return retorno;
//	}
//
//	@RequestMapping(value = "", method = RequestMethod.PUT, consumes = JSON_TYPE)
//	public ResponseEntity<Response> update(@RequestBody TipoMaterial o) {
//		service.update(o);
//		Response resp = new Response();
//		ResponseEntity<Response> retorno = new ResponseEntity<Response>(resp, HttpStatus.OK);
//		return retorno;
//	}
//
//	@RequestMapping(value = "", method = RequestMethod.DELETE, consumes = JSON_TYPE)
//	public ResponseEntity<Response> delete(@RequestBody TipoMaterial o) {
//		service.delete(o);
//		Response resp = new Response();
//		ResponseEntity<Response> retorno = new ResponseEntity<Response>(resp, HttpStatus.OK);
//		return retorno;
//	}
//
//}
