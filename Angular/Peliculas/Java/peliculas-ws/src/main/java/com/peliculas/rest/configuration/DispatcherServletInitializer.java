package com.peliculas.rest.configuration;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class DispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
 
	private int MAX_UPLOAD_SIZE = 5 * 1024 * 1024;
	
    @Override
    protected Class<?>[] getRootConfigClasses() {
    	return new Class[]{};
    }
  
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {};
    }
  
    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }
    
    @Override
    protected Filter[] getServletFilters() {
    	Filter [] singleton = { new CORSFilter(), new MultipartFilter(), new OpenEntityManagerInViewFilter()};
    	return singleton;
    }

	@Override
	protected void customizeRegistration(Dynamic registration) {
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(".", 
		          MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE / 2);
		registration.setMultipartConfig(multipartConfigElement);
		super.customizeRegistration(registration);
	}
    


    
    
 
}