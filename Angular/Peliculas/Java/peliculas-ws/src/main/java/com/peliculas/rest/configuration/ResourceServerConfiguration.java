package com.peliculas.rest.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
@PropertySource("classpath:seguridad.properties")
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	
	@Value("${oauth.resourceId}")
	private String RESOURCE_ID;
	

	@Value("${ldap.mainGroup}")
	private String mainGroup;
	
	@Value("${oauth.servicesContext}")
	private String servicesContext;
	
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).stateless(false);
		
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.
		anonymous().disable()
		.requestMatchers().antMatchers(servicesContext)
		.and().authorizeRequests()
		.antMatchers(servicesContext).access("hasRole('"+mainGroup+"')")
		.and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}

}