package com.peliculas.rest.configuration;

import javax.naming.Name;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;


import org.springframework.security.ldap.DefaultLdapUsernameToDnMapper;
import org.springframework.security.ldap.LdapUsernameToDnMapper;
import org.springframework.security.ldap.userdetails.LdapUserDetailsManager;



@Configuration
@EnableWebSecurity
@PropertySource("classpath:seguridad.properties")
public class ServerSecurityConfig extends WebSecurityConfigurerAdapter  {
	
	@Value("${ldap.userDnBase}")
	private String userDnBase;
	
	@Value("${ldap.userSearchAttribute}")
	private String userSearchAttribute;
	
	@Value("${ldap.groupSearchBase}")
	private String groupSearchBase;
	
	@Value("${ldap.url}")
	private String ldapUrl;
	
	@Value("${ldap.adminDN}")
	private String adminDN;
	
	@Value("${ldap.adminPassword}")
	private String adminPassword;
	
	@Value("${oauth.tokenService}")
	private String tokenService;
	
	

	@Autowired
	private ClientDetailsService clientDetailsService;
	
	
	
	
	
	@Bean
	public UserDetailsService userDetailsService() {
		
		LdapUsernameToDnMapper usernameMapper = new DefaultLdapUsernameToDnMapper(userDnBase,
				userSearchAttribute);
		
		
		LdapUserDetailsManager m1 = new LdapUserDetailsManager(contextSource());
		m1.setGroupSearchBase(groupSearchBase);
		m1.setUsernameMapper(usernameMapper);
		return m1;
		

		
	}
		
	 
	    public LdapContextSource contextSource() {
	        LdapContextSource ct= new LdapContextSource();
	        ct.setUrl(ldapUrl);
	        ct.setUserDn(adminDN);
	        ct.setPassword(adminPassword);
	        ct.afterPropertiesSet();
	       
	        return ct;
	    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		
		http
		.csrf().disable()
		.anonymous().disable()
	  	.authorizeRequests()
	  	.antMatchers(HttpMethod.OPTIONS,"**").permitAll()
	  	.antMatchers(tokenService).permitAll();
		
		
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS);
    }
	
	@Override
    protected void configure(AuthenticationManagerBuilder auth) 
      throws Exception {
		
		auth.userDetailsService(userDetailsService());

    }

	@Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}

	@Bean
	@Autowired
	public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore){
		TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
		handler.setTokenStore(tokenStore);	
		handler.setRequestFactory(new DefaultOAuth2RequestFactory(clientDetailsService));
		handler.setClientDetailsService(clientDetailsService);
		
		return handler;
	}
	
	@Bean
	@Autowired
	public ApprovalStore approvalStore(TokenStore tokenStore) throws Exception {
		TokenApprovalStore store = new TokenApprovalStore();
		store.setTokenStore(tokenStore);
		return store;
	}

}