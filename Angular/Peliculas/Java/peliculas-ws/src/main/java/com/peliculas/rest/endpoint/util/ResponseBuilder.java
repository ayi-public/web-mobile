package com.peliculas.rest.endpoint.util;

public class ResponseBuilder<T>
{
	private Response<T> Response;
	
	public ResponseBuilder()
	{
		Response = new Response<T>();
	}
	
	public ResponseBuilder<T> ok(){
		Response.setStatus(true);
		return this;
	}
	
	public ResponseBuilder<T> notOk(){
		Response.setStatus(false);
		return this;
	}
	
	public ResponseBuilder<T> message(String msg){
		Response.setMessage(msg);
		return this;
	}
	
	public ResponseBuilder<T> error(String error){
		Response.addError(error);
		return this;
	}
	
	public Response<T> build()
	{
		return Response;
	}

}
