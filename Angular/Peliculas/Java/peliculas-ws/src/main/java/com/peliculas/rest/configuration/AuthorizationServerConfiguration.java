package com.peliculas.rest.configuration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;


@Configuration
@EnableAuthorizationServer
@PropertySource("classpath:seguridad.properties")
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

	private static Logger logger =  Logger.getLogger(AuthorizationServerConfiguration.class);
	
	@Value("${oauth.tokenValidity}")
	private String tokenValidity;
	
	@Value("${oauth.tokenRefreshValidity}")
	private String tokenRefreshValidity;
	
	@Value("${oauth.realmName}")
	private String REALM;
	
	@Value("${oauth.clientName}")
	private String clientName;
	
	@Value("${oauth.clientPassword}")
	private String clientPassword;
	
	@Value("${oauth.resourceId}")
	private String resourceId;
	
	
	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private UserApprovalHandler userApprovalHandler;

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;
	

	

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		logger.debug("configure...");
		clients.inMemory()
	        .withClient(clientName)
            .authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit")
            .authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
            .scopes("read", "write", "trust")
            .secret(clientPassword)
            .resourceIds(resourceId)
            .accessTokenValiditySeconds(Integer.parseInt(tokenValidity)).//Access token is only valid for 2 minutes.
            refreshTokenValiditySeconds(Integer.parseInt(tokenRefreshValidity));//Refresh token is only valid for 10 minutes.
	
	}

	
	public DefaultTokenServices tokenServices() {
        //final MyTokenServices defaultTokenServices = new MyTokenServices();
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore);
        defaultTokenServices.setSupportRefreshToken(true);
        defaultTokenServices.setRefreshTokenValiditySeconds(Integer.parseInt(tokenRefreshValidity));
        defaultTokenServices.setAccessTokenValiditySeconds(Integer.parseInt(tokenValidity));
        
        return defaultTokenServices;
    }
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(tokenStore)
		.userApprovalHandler(userApprovalHandler)
		.authenticationManager(authenticationManager);
		
		endpoints.tokenServices(tokenServices());
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.passwordEncoder(passwordEncoder() );
		oauthServer.realm(REALM+"/client");
		
		
	}
	
			@Bean
			public PasswordEncoder passwordEncoder() {
				PasswordEncoder encoder =  NoOpPasswordEncoder.getInstance();
				return encoder;
			}
	

}