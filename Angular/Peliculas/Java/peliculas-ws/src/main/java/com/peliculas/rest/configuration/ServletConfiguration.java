package com.peliculas.rest.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@ImportResource(value = {"classpath:dispatcher-servlet.xml"})
@Configuration
@EnableWebMvc
//@ComponentScan(basePackages = "com.ider.rest")
public class ServletConfiguration {

}
