package com.peliculas.rest.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

	public SecurityWebApplicationInitializer() {
		super( ServerSecurityConfig.class, AuthorizationServerConfiguration.class, ResourceServerConfiguration.class,ServletConfiguration.class);
	}
}