package com.peliculas.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.peliculas.business.repository.UnidadesMedidaRepository;
import com.peliculas.entities.UnidadesMedida;

@Service
@Transactional( readOnly = true )
public class UnidadesMedidaService {
	@Autowired
	private UnidadesMedidaRepository repository;


	public List<UnidadesMedida> findAll() {
		return repository.findAll();
	}

	@Transactional
	public void create(UnidadesMedida o) {
		repository.save(o);
	}

	@Transactional
	public void update(UnidadesMedida o) {
		repository.save(o);
	}

	@Transactional
	public void delete(UnidadesMedida o) {
		repository.delete(o);
	}

}
