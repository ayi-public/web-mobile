package com.peliculas.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.peliculas.entities.UnidadesMedida;

public interface UnidadesMedidaRepository extends JpaRepository<UnidadesMedida, Integer> {
}
