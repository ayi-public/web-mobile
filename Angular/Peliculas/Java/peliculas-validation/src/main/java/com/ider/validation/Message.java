package com.ider.validation;

public class Message {
	private String message;
	public static enum MESSAGE_LEVEL {INFO, WARNING, ERROR};

	private String level;
	
	public Message(){
		
	}
	public Message(Message.MESSAGE_LEVEL level, String message){
		this.level = level.toString();
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
	
	


}
