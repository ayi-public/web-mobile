package com.ider.metrics.rest.endpoint.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	
	@ExceptionHandler
	protected ResponseEntity<Response> handleExceptions(Exception ex, WebRequest request) {
		
		
		Response resp = new Response();
		
		
		
		resp.addError(ex.getMessage());
		resp.setStatus(false);
		
		
		ResponseEntity<Response> retorno = 
				new ResponseEntity<Response>(resp, HttpStatus.OK);
		return retorno;
	}
}