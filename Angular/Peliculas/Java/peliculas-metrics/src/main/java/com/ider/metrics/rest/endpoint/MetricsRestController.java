package com.ider.metrics.rest.endpoint;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ider.metrics.util.MetricsInstaller;



@RestController
@RequestMapping("/metricas")
public class MetricsRestController {

	private static final String JSON_TYPE = MediaType.APPLICATION_JSON_VALUE;


	@RequestMapping(value = "/count/{metrica}/cantidad/{cantidad}", method = RequestMethod.GET, produces = JSON_TYPE)
	public ResponseEntity contar(@PathVariable("metrica") String metrica,@PathVariable("cantidad") Integer cantidad) {
		
		MetricsInstaller.count(metrica, cantidad);
		
		ResponseEntity response = new ResponseEntity(HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value = "/timer/{metrica}/start", method = RequestMethod.GET, produces = JSON_TYPE)
	public ResponseEntity timerStart(@PathVariable("metrica") String metrica) {
		MetricsInstaller.timeStart(metrica);
		
		ResponseEntity response = new ResponseEntity(HttpStatus.OK);
		return response;
	}
	@RequestMapping(value = "/timer/{metrica}/stop", method = RequestMethod.GET, produces = JSON_TYPE)
	public ResponseEntity timerStop(@PathVariable("metrica") String metrica) {
		
		MetricsInstaller.timeStop(metrica);
		
		
		ResponseEntity response = new ResponseEntity(HttpStatus.OK);
		return response;
	}
	
//	@RequestMapping(value = "", method = RequestMethod.POST, consumes = JSON_TYPE)
//	public ResponseEntity<Response> create(@RequestBody Recepcion o) {
//		System.out.println(o);
//		service.create(o);
//		Response resp = new Response();
//		ResponseEntity<Response> retorno = new ResponseEntity<Response>(resp, HttpStatus.OK);
//		return retorno;
//	}
}
