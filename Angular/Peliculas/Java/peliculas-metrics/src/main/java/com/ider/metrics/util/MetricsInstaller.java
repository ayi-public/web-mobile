package com.ider.metrics.util;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.servlets.HealthCheckServlet;
import com.codahale.metrics.servlets.MetricsServlet;

public class MetricsInstaller implements ServletContextListener  {

	 /** La constante METRIC_REGISTRY. */
    public static final MetricRegistry METRIC_REGISTRY = new MetricRegistry();

    /** La constante HEALTH_CHECK_REGISTRY. */
    public static final HealthCheckRegistry HEALTH_CHECK_REGISTRY = new HealthCheckRegistry();
    
    private static Map metricas = new HashMap();


    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {

        initMetrics(servletContextEvent.getServletContext());
    }

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	 private void initMetrics(final ServletContext servletContext) {

	        servletContext.setAttribute(MetricsServlet.METRICS_REGISTRY, METRIC_REGISTRY);
	        servletContext.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, HEALTH_CHECK_REGISTRY);
	        
	        final Graphite graphite = new Graphite(new InetSocketAddress("180.120.10.50", 2003));
	        final GraphiteReporter reporter = GraphiteReporter.forRegistry(this.METRIC_REGISTRY)
	                                                          .prefixedWith("www.ider.com")
	                                                          .convertRatesTo(TimeUnit.SECONDS)
	                                                          .convertDurationsTo(TimeUnit.MILLISECONDS)
	                                                          .filter(MetricFilter.ALL)
	                                                          .build(graphite);
	        reporter.start(1, TimeUnit.MINUTES);
	        

	    }
	 public static void count(final String mensaje, final Integer cantidad) {
	        METRIC_REGISTRY.counter(mensaje).inc(cantidad);

	    }
	 
	

	    public static Timer.Context timeStart(final String mensaje) {
	    	Timer.Context timer = METRIC_REGISTRY.timer(mensaje).time();
	    	metricas.put(mensaje, timer);
	        return timer;
	    }

	    public static void timeStop(final String mensaje) {
	        Timer.Context tc = (Timer.Context) metricas.get(mensaje);
	        tc.stop();
	    }
	 
}
