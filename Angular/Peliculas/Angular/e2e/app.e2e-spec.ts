import {AppPage} from './app.po';
import {browser} from 'protractor';

describe('login App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('página de logueo', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Usuario:');
  });

  it('completar el campo usuario', () => {
    page.navigateTo();
    page.getInputUsuario().sendKeys('14523');
    expect(page.getInputUsuario().getAttribute('value')).toEqual('14523');
  });

  it('completar el campo contraseña', () => {
    page.navigateTo();
    page.getInputContraseña().sendKeys('clave');
    expect(page.getInputContraseña().getAttribute('value')).toEqual('clave');
  });

  it('loguearse correctamente', () => {
    page.navigateTo();
    page.getInputUsuario().sendKeys('14523');
    page.getInputContraseña().sendKeys('clave');
    page.getButtonAceptar().click();
    expect(page.getListadoPeliculas()).toEqual('Agregar pelicula');
  });

  it('agregar pelicula', () => {
    page.getNroIdentificatorio().sendKeys('10');
    page.getNombre().sendKeys('pruebaE2E');
    page.getDirector().sendKeys('pruebaE2E');
    page.getGenero().$('[value="drama"]').click();
    expect(page.getGenero().getAttribute('value')).toEqual('drama');
    page.getAgregarPelicula().click();
    page.getBotonSiguiente().click();
    expect(page.getNuevoRegistro()).toBeDefined();
    page.getGuardarLista().click();
    browser.pause();
  });
});
