import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-login label')).getText();
  }

  getInputUsuario() {
    return element(by.xpath('//*[@id="usuario"]'));
  }

  getInputContraseña() {
    return element(by.xpath('//*[@id="password"]'));
  }

  getButtonAceptar() {
    return element(by.css('app-login button'));
  }

  getListadoPeliculas() {
    return element(by.xpath('/html/body/app-root/app-listado-peliculas/div/div[1]/div/app-agregar-pelicula/div/h1')).getText();
  }

  getNroIdentificatorio() {
    return element(by.css('[id="input-id"]'));
  }

  getNombre() {
    return element(by.css('[id="input-nombre"]'));
  }

  getDirector() {
    return element(by.css('[id="input-director"]'));
  }

  getGenero() {
    return element(by.css('[id="select-genero"]'));
  }

  getAgregarPelicula() {
    return element(by.css('[id="button-peliculas"]'));
  }

  getBotonSiguiente() {
    return element(by.xpath('/html/body/app-root/app-listado-peliculas/div/div[2]/div/pagination-controls/pagination-template/ul/li[4]/a'));
  }

  getNuevoRegistro() {
    return element(by.xpath('/html/body/app-root/app-listado-peliculas/div/div[2]/div/table/tbody/tr[3]/td[1]'));
  }

  getGuardarLista() {
    return element(by.xpath('/html/body/app-root/app-listado-peliculas/div/div[2]/div/button'));
  }
}
