import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { Seguridad } from './services/seguridad.service';
import { HttpClientModule } from '@angular/common/http';
import { Pag2Component } from './pag2/pag2.component';
import {LogoutService} from './services/logout.service';
import { RouterModule, Routes } from '@angular/router';
import { AGREGARPELICULA, LISTADOPELICULAS, LOGIN } from './shared/constantes';
import { RolGuard } from './shared/logged-in.guard';
import { ListadoPeliculasComponent } from './listado-peliculas/listado-peliculas.component';
import { CookieService } from 'ngx-cookie-service';
import { ModalModule } from 'ngx-bootstrap';
import { ErrorComponent } from './error/error.component';
import { AgregarPeliculaComponent } from './agregar-pelicula/agregar-pelicula.component';
import { ActualizarPeliculaComponent } from './actualizar-pelicula/actualizar-pelicula.component';
import { NgxPaginationModule } from 'ngx-pagination';

const ROUTES: Routes = [
  { path: LOGIN, component: LoginComponent },
  {
    path: LISTADOPELICULAS, component: ListadoPeliculasComponent,
    canActivate: [RolGuard]
  },
  {
    path: AGREGARPELICULA, component: AgregarPeliculaComponent,
    canActivate: [RolGuard]
  },
  { path: '**', redirectTo: '', pathMatch: 'full', component: ErrorComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Pag2Component,
    ListadoPeliculasComponent,
    ErrorComponent,
    AgregarPeliculaComponent,
    ActualizarPeliculaComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    ModalModule.forRoot(),
    NgxPaginationModule
  ],
  providers: [
    Seguridad,
    CookieService,
    RolGuard,
    LogoutService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
