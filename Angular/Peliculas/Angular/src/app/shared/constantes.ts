export const PAGINA_2 = 'pagina2';
export const LOGIN = 'login';
export const LISTADOPELICULAS = 'listado-peliculas';
export const AGREGARPELICULA = 'agregar-pelicula';

export const listadoPeliculas = {
  peliculas: [
    {
      id: 1,
      nombre: 'El sexto sentido',
      director: 'M. Night Shyamalan',
      clasificacion: 'Drama'
    },
    {
      id: 2,
      nombre: 'Pulp Fiction',
      director: 'Tarantino',
      clasificacion: 'Acción'
    },
    {
      id: 3,
      nombre: 'Todo Sobre Mi Madre',
      director: 'Almodobar',
      clasificacion: 'Drama'
    },
    {
      id: 4,
      nombre: '300',
      director: 'Zack Snyder',
      clasificacion: 'Acción'
    },
    {
      id: 5,
      nombre: 'El silencio de los corderos',
      director: 'Jonathan Demme',
      clasificacion: 'Drama'
    },
    {
      id: 6,
      nombre: 'Forrest Gump',
      director: 'Robert Zemeckis',
      clasificacion: 'Comedia'
    },
    {
      id: 7,
      nombre: 'Las Hurdes',
      director: 'Luis Buñuel',
      clasificacion: 'Documental'
    }
  ]
};

// VALORES SERVICIO DE COOKIES
export const USUARIO = 'usuario';
export const ACCESS_TOKEN = 'access_token';
export const REFRESH_TOKEN = 'refresh_token';
