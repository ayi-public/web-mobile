import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {Seguridad} from '../services/seguridad.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class RolGuard implements CanActivate {
  urlValida = false;
  constructor(private user: Seguridad,
              private cookieService: CookieService) {}

  canActivate(route: ActivatedRouteSnapshot) {
    /*this.urlValida = false;
    if (this.obtenerUsuario() === null) {
        return false;
    } else {
        for (const i of this.obtenerUsuario().menu) {
            for (const j of i.aplicaciones) {
                if (route.data.url === j.url ) {
                    this.urlValida = true;
                    break;
                }
            }
        }
        return (this.urlValida);
    }*/
    return true;
  }
  public obtenerUsuario(): any {
    if  (this.cookieService.get('usuario') === '') {
      return null;
    } else {
      return JSON.parse(this.cookieService.get('usuario'));
    }
  }
}
