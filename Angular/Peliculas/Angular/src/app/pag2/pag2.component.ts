import {Component, OnInit} from '@angular/core';
import {LogoutService} from '../services/logout.service';
import {Router} from '@angular/router';
import {LOGIN} from '../shared/constantes';

@Component({
  selector: 'app-pag2',
  templateUrl: './pag2.component.html',
  styleUrls: ['./pag2.component.css']
})
export class Pag2Component implements OnInit {

  constructor( private router: Router,
               private logoutService: LogoutService) {
  }

  ngOnInit() {
  }

  logout() {
    console.log('cerrar sesion');
    // LLAMAR AL SERVICIO PARA DESLOGUEARSE
    this.router.navigate(['/' + LOGIN]);
  }

}
