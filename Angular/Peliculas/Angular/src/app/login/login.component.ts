import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Seguridad } from '../services/seguridad.service';
import { Router } from '@angular/router';
import { LISTADOPELICULAS, PAGINA_2 } from '../shared/constantes';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  usuarioClaveIncorrecta = false;

  constructor(public formBuilder: FormBuilder,
              private router: Router,
              private services: Seguridad) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usuario: [{ value: null, disabled: false }, []],
      password: [{ value: null, disabled: false }, []]
    });
  }

  login() {
    // this.services.servicio(this.loginForm.get('usuario').value, this.loginForm.get('password').value); // .subscribe(
    // res => {
    //   this.loginForm.patchValue({
    //     usuario: null,
    //     password: null
    //   });
    //
    this.router.navigate(['/' + LISTADOPELICULAS]);
    // }, error => {
    //   this.usuarioClaveIncorrecta = true;
    // });
  }

}
