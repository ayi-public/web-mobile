import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ListadoPeliculasComponent} from './listado-peliculas.component';
import {BsModalService, ModalModule} from 'ngx-bootstrap';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';
import {By} from '@angular/platform-browser';

describe('ListadoPeliculasComponent', () => {
  let fixture: ComponentFixture<ListadoPeliculasComponent>;
  let instance: ListadoPeliculasComponent;
  let listadoPeliculasComponent: ListadoPeliculasComponent;
  let modal: BsModalService;
  let template: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListadoPeliculasComponent],
      providers: [
        BsModalService
      ],
      imports: [
        ModalModule.forRoot(),
        NgxPaginationModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(ListadoPeliculasComponent);
    instance = fixture.componentInstance;

    listadoPeliculasComponent = fixture.debugElement.injector.get(ListadoPeliculasComponent);

    template = fixture.debugElement.queryAll(By.css('ng-template'));
    modal = fixture.debugElement.injector.get(BsModalService);
  });

  it('cargar lista peliculas', () => {
    instance.ngOnInit();
    expect(instance.listadoPeliculas.length).toBe(7);
  });

  it('agregar pelicula', () => {
    const pelicula = {
      clasificacion: 'drama',
      director: 'Test',
      id: 41,
      nombre: 'Test'
    };
    instance.ngOnInit();
    instance.agregarPelicula(pelicula);
    expect(instance.listadoPeliculas.length).toBe(8);
    expect(instance.listadoPeliculas[7].clasificacion).toBe('drama');
  });

  it('editar pelicula', () => {
    const pelicula = {
      clasificacion: 'drama',
      director: 'TestEdit',
      id: 7,
      nombre: 'TestEdit'
    };
    instance.ngOnInit();
    instance.modalRef = modal.show(template[1]);
    instance.guardarPeliculaEditada(pelicula);
    expect(instance.listadoPeliculas.length).toBe(8);
    expect(instance.listadoPeliculas[6].nombre).toBe('TestEdit');
  });

  it('eliminar pelicula', () => {
    const pelicula = {
      clasificacion: 'drama',
      director: 'TestEdit',
      id: 7,
      nombre: 'TestEdit'
    };
    instance.ngOnInit();
    instance.modalRef = modal.show(template[0]);
    instance.peliculaAEliminar = pelicula;
    instance.eliminarPelicula();
    expect(instance.listadoPeliculas.length).toBe(7);
  });

  it('abrir un modal', () => {
    const pelicula = {
      clasificacion: 'drama',
      director: 'TestEdit',
      id: 7,
      nombre: 'TestEdit'
    };
    instance.abrirModal(template[1], pelicula);
    expect(instance.peliculaAEliminar).toBeDefined();
    expect(instance.peliculaAEditar).toBeDefined();
  });
});
