import { Component, OnInit, TemplateRef } from '@angular/core';
import { listadoPeliculas } from '../shared/constantes';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-listado-peliculas',
  templateUrl: './listado-peliculas.component.html',
  styleUrls: ['./listado-peliculas.component.css']
})
export class ListadoPeliculasComponent implements OnInit {

  listadoPeliculas;
  peliculaAEliminar;
  peliculaAEditar;

  modalRef: BsModalRef;

  constructor(private modalService: BsModalService) {
  }

  ngOnInit() {
    this.listadoPeliculas = listadoPeliculas.peliculas;
    this.ordenarPeliculas();
  }

  ordenarPeliculas() {
    this.listadoPeliculas.sort((a, b) => a.id - b.id);
  }

  agregarPelicula(event) {
    this.listadoPeliculas.push(event);
    this.ordenarPeliculas();
  }

  guardarPeliculaEditada(event) {
    for ( let i = 0; i < this.listadoPeliculas.length; i++ ) {
      if ( this.listadoPeliculas[i].id === event.id ) {
        this.listadoPeliculas[i].nombre = event.nombre;
        this.listadoPeliculas[i].director = event.director;
        this.listadoPeliculas[i].clasificacion = event.clasificacion;
        break;
      }
    }
    this.modalRef.hide();
  }

  eliminarPelicula() {
    for ( let i = 0; i < this.listadoPeliculas.length; i++ ) {
      if ( this.listadoPeliculas[i].id === this.peliculaAEliminar.id ) {
        this.listadoPeliculas.splice(i, 1);
        break;
      }
    }
    this.modalRef.hide();
  }

  abrirModal(template: TemplateRef<any>, pelicula) {
    this.modalRef = this.modalService.show(template);
    this.peliculaAEliminar = pelicula;
    this.peliculaAEditar = pelicula;
  }
}
