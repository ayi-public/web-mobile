import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {USUARIO, ACCESS_TOKEN, REFRESH_TOKEN} from '../shared/constantes';

@Injectable()
export class LogoutService {
  public mensaje: string;

  constructor(
    // private httpClient: HttpClient,
              private cookieService: CookieService
  ) {
  }

  // private getURL(): string {
  //   const url: string = window.location.origin;
  //   const urlServidor = url.replace('4200', '7101'); // PARA DESARROLLO
  //   // const urlServidor = url.replace('4200', '7001'); // PARA DESARROLLO
  //   return urlServidor + URL_LOGOUT_SERVLET;
  // }
  //
  // private getHeaders(): HttpHeaders {
  //   const headers = new HttpHeaders();
  //   return headers;
  // }
  //
  // public invocarServicio(): Observable<string> {
  //   this.mensaje = null;
  //   const url = this.getURL();
  //   const body = '{}';
  //   const headers = this.getHeaders();
  //   return this.httpClient.post(url, body, { headers: headers })
  //     .map(resp => {
  //       this.logout();
  //       return null;
  //     }).catch(this.handleError);
  // }
  //
  // private handleError(res: any): Observable<any> {
  //   console.dir(res);
  //   const errMsg = 'Ocurrió un error en el servicio de logout';
  //   console.error(errMsg);
  //   return Observable.throw(errMsg);
  // }

  public logout() {
    // SE ELIMINAN LAS COOKIES
    this.cookieService.delete(USUARIO, '/');
    this.cookieService.delete(ACCESS_TOKEN, '/');
    this.cookieService.delete(REFRESH_TOKEN, '/');
  }


}
