import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class Seguridad implements OnInit {


  dataToken: any;
  urlToken = '';

  public ngOnInit() {

  }
  constructor(private httpClient: HttpClient,
              public cookieService: CookieService) { }


  public servicio(usuario: string, password: string) { // : Observable<string> {

    // const url = this.urlToken;
    // const body = '{}';
    // const headers = this.getHeaders();
    //
    // const parametros = this.getHttpParams(usuario, password);
    //
    // this.cookieService.set('usuario', usuario);
    //
    // return this.httpClient.post(url, body, { headers: headers, params: parametros, withCredentials: true })
    //   .map(
    //     res => {
    //       this.cookieService.set('access_token', res['access_token'].toString(), res['expires_in']);
    //       this.cookieService.set('refresh_token', res['refresh_token'].toString());
    //       return res;
    //     }).catch(error => {
    //
    //     return Observable.throw(error);
    //   });
    console.log('servicio Usuario, Clave');
  }

  private handleError(res: any): Observable<any> {
    const errMsg = 'Ocurrió un error en el servicio de obtención de token';
    return Observable.throw(errMsg);
  }

  private getHttpParams(usuario, password): HttpParams {
    let parametros = new HttpParams();
    parametros = parametros.append('grant_type', 'password');
    parametros = parametros.append('username', usuario);
    parametros = parametros.append('password', password);

    return parametros;
  }

  private getHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Basic ' + btoa('my-trusted-client:secret'));
    headers = headers.append('Content-Type', 'application/json;charset=UTF-8');
    headers = headers.append('Accept', 'application/json');
    return headers;
  }

  public getTokenRefresh(): Observable<string> {

    const url = this.urlToken;
    const body = '{}';
    const headers = this.getHeaders();

    const parametros = this.getHttpRefreshParams();

    return this.httpClient.post(url, body, { headers: headers, params: parametros, withCredentials: true })
      .map(
        res => {
          this.cookieService.set('access_token', res['access_token'].toString(), res['expires_in']);
          this.cookieService.set('refresh_token', res['refresh_token'].toString());
          return res;
        }).catch(error => {
        console.log('error al refrescar el token');
        console.log(error);
        return Observable.throw(error);
      });

  }

  private getHttpRefreshParams(): HttpParams {
    let parametros = new HttpParams();
    const refresh_token = this.cookieService.get('refresh_token');
    parametros = parametros.append('grant_type', 'refresh_token');
    parametros = parametros.append('refresh_token', refresh_token);

    return parametros;
  }


}
