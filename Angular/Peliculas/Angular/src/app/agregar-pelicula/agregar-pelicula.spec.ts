import {ComponentFixture, TestBed} from '@angular/core/testing';

import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AgregarPeliculaComponent} from './agregar-pelicula.component';
import {FormBuilder, ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';

describe('ListadoPeliculasComponent', () => {
  let fixture: ComponentFixture<AgregarPeliculaComponent>;
  let instance: AgregarPeliculaComponent;
  let button: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AgregarPeliculaComponent],
      providers: [
        FormBuilder
      ],
      imports: [
        ReactiveFormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(AgregarPeliculaComponent);
    instance = fixture.componentInstance;

    button = fixture.debugElement.query(By.css('button'));

  });

  it('cargar lista peliculas', () => {
    instance.ngOnInit();
    expect(instance.listadoPeliculas).toBeDefined();
  });

  // it('validarId', async(() => {
  //   instance.ngOnInit();
  //   expect(validarId(new FormControl('123'))).toEqual({'validarIdentificacion': true});
  //
  // }));

  // it('agregar pelicula', () => {
  //   const pelicula = {
  //     clasificacion: 'drama',
  //     director: 'Test',
  //     id: 41,
  //     nombre: 'Test'
  //   };
  //   spyOn(instance.nuevaPeli, 'emit');
  //   button.nativeElement.click();
  //   fixture.detectChanges();
  //   expect(instance.nuevaPeli.emit(pelicula)).toHaveBeenCalled();
  // });

});
