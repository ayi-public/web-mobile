import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { listadoPeliculas } from '../shared/constantes';

@Component({
  selector: 'app-agregar-pelicula',
  templateUrl: './agregar-pelicula.component.html',
  styleUrls: ['./agregar-pelicula.component.css']
})
export class AgregarPeliculaComponent implements OnInit {

  @Output('nuevaPeli') nuevaPeli = new EventEmitter();
  agregarPeliculaForm: FormGroup;
  listadoPeliculas;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.listadoPeliculas = listadoPeliculas.peliculas;
    this.agregarPeliculaForm = this.formBuilder.group({
      'input-id': ['', [Validators.required, Validators.pattern('[0-9]{1,5}')]],
      'input-nombre': ['', Validators.required],
      'input-director': ['', Validators.required],
      'select-genero': ['', Validators.required]
    });
  }

  agregarPelicula() {
    const pelicula = {
      id: this.agregarPeliculaForm.value['input-id'],
      nombre: this.agregarPeliculaForm.value['input-nombre'],
      director: this.agregarPeliculaForm.value['input-director'],
      clasificacion: this.agregarPeliculaForm.value['select-genero']
    };
    this.nuevaPeli.emit(pelicula);
  }

}

// export function validarId(c: AbstractControl) {
//   console.log(c)
//   for ( let i = 0; i < this.listadoPeliculas.peliculas.length; i++ ) {
//     if ( c.get('input-id') === this.listadoPeliculas.peliculas[i] ) {
//       return { validarIdentificacion: true };
//     } else {
//       return null;
//     }
//   }
// }
