import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-actualizar-pelicula',
  templateUrl: './actualizar-pelicula.component.html',
  styleUrls: ['./actualizar-pelicula.component.css']
})
export class ActualizarPeliculaComponent implements OnInit {

  @Output('cerrar') cerrar = new EventEmitter();
  @Output('peliEditada') peliEditada = new EventEmitter();
  @Input() peliculaAEditar;

  editarPeliculaForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.editarPeliculaForm = this.formBuilder.group({
      'input-id': ['', [Validators.required, Validators.pattern('[0-9]{1,5}')]],
      'input-nombre': ['', Validators.required],
      'input-director': ['', Validators.required],
      'select-genero': ['', Validators.required]
    });
    this.llenarFormulario();
  }

  llenarFormulario() {
    this.editarPeliculaForm.setValue({
      'input-id': this.peliculaAEditar.id,
      'input-nombre': this.peliculaAEditar.nombre,
      'input-director': this.peliculaAEditar.director,
      'select-genero': this.peliculaAEditar.clasificacion

    });
  }

  cerrarModalEditar() {
    this.cerrar.emit(null);
  }

  guardarPelicula() {
    const pelicula = {
      id: this.editarPeliculaForm.value['input-id'],
      nombre: this.editarPeliculaForm.value['input-nombre'],
      director: this.editarPeliculaForm.value['input-director'],
      clasificacion: this.editarPeliculaForm.value['select-genero']
    };
    this.peliEditada.emit(pelicula);
  }

}
